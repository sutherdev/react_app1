import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

//following Road to Learn React

const list = [
  {
  title: 'React',
  url: 'https://facebook.github.io/react/',
  author: 'Jordan Walke',
  num_comments: 3,
  points: 4,
  objectID: 0,
  },
  {
  title: 'Redux',
  url: 'https://github.com/reactjs/redux',
  author: 'Dan Abramov, Andrew Clark',
  num_comments: 2,
  points: 5,
  objectID: 1,
  },
];

//function is higher order, function which returns another function 
//needs to be outside of component (p 54 in book)

//ES5

function isSearched(searchTerm){
  return function(item){
    return item.title.toLowerCase().includes(searchTerm.toLowerCase());
  }
}


//ES6
/*
const isSearched = searchTerm => item => 
  item.title.toLowerCase().includes(searchTerm.toLowerCase());
*/

class App extends Component {

  constructor(props){
    super(props);

    this.state = {
      //list: list,
      list,
      searchTerm: '',
    }
    //binding needed so 'this' is set to component.  otherwise, use of 'this' can go undefined when instantiated
    this.onDismiss = this.onDismiss.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
  }

  onSearchChange(event) {
    //event is synthetic (React version of 'event')
    this.setState({searchTerm: event.target.value});
  }

  onDismiss(id){
    //returns all list items EXCEPT for id (which is filtered out)

    //3 different blocks below all different ways of doing the same thing
    //1
    /*
    function isNotId(item) {
      return item.objectID !== id;
    }
    */

    //2
    /*
    const isNotId = item => (item.objectID != id);
    const updatedList = this.state.list.filter(isNotId);
    */

    //3 
    const updatedList = this.state.list.filter(item => (item.objectID !== id));


    this.setState({list: updatedList});

  }
  

  render() {

    //deconstructed object, saves on stating this.state.  in map loop
    const { searchTerm, list } = this.state;

    return (
      <div className="App"><img src={logo} className="App-logo" alt="logo" />
        <br/><br/><br/><br/><br/><br/>


        <Search 
          onChange={this.onSearchChange}
          value={searchTerm}
        >
          Search
        </Search>

        <Table 
          list={list}
          pattern={searchTerm}
          onDismiss={this.onDismiss}
        />

      </div>
    );
    
  }
}


const Search = ({value, onChange, children}) =>
  <form>
    <span>{children}: </span> 
    <input 
      type="text"
      onChange={onChange}
      value={value}
    />
  </form>



const Table = ({list, pattern, onDismiss}) => 
  <div>
    {list.filter(isSearched(pattern)).map(item =>
      <div key={item.objectID}>
        <span>
          <a href={item.url}>{item.title}</a>
        </span>&nbsp;
        <span>{item.author}</span>
        <span>{item.num_comments}</span>
        <span>{item.points}</span>&nbsp;
          <span>
            <Button onClick={() => onDismiss(item.objectID)}>
              Dismiss
            </Button>
        </span>

      </div>
    )}
  </div>


const Button = ({onClick, className ='', children}) => 
  <button
    onClick={onClick}
    className={className}
    type="button"
  >
    {children}
  </button>


/*
class Search extends Component {
  render(){
    const {onChange, value, children} = this.props;

    return (
      // check component examples in other repo 
      <form>
        <span>{children}: </span> 
        <input 
          type="text"
          onChange={onChange}
          value={value}
        />
      </form>
    )
  }
}

class Table extends Component {
   
  render() {
    const {list, pattern, onDismiss} = this.props;

    return (

      <div>
        {list.filter(isSearched(pattern)).map(item =>
          <div key={item.objectID}>
            <span>
              <a href={item.url}>{item.title}</a>
            </span>&nbsp;
            <span>{item.author}</span>
            <span>{item.num_comments}</span>
            <span>{item.points}</span>&nbsp;
              <span>
                <Button onClick={() => onDismiss(item.objectID)}>
                  Dismiss
                </Button>
            </span>

          </div>
        )}
      </div>
    )
  }
}


class Button extends Component {
  render() {
    const {
      onClick,
      className ='',
      children,
    } = this.props;

    return (
      <button
        onClick={onClick}
        className={className}
        type="button"
      >
        {children}
      </button>
    )
  }
}
*/


export default App;
